const iconPassword = document.querySelectorAll(".icon-password");
const password = document.querySelectorAll(".password");

iconPassword.forEach(function (toggle, index) {
  toggle.addEventListener("click", function () {
    const type =
      password[index].getAttribute("type") === "password" ? "text" : "password";
    password[index].setAttribute("type", type);
    this.classList.toggle("fa-eye-slash");
  });
});

const btn = document.querySelector(".btn");
function submitForm() {
  const validPassword = document.querySelector(".password").value;
  const confirmPassword = document.querySelector(".confirmPassword").value;

  if (validPassword === confirmPassword) {
    alert("You are welcome!");
  } else {
    alert("Потрібно ввести однакові значення");
  }
}
btn.addEventListener("click", submitForm);
